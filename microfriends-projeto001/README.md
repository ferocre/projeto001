1) Criar aplicação com menu:
  - Cadastrar novo fundo
    - Qual tipo do fundo ? (ACAO(1), MULTI-MERCADO(2), RENDA-FIXA(3)
    - Qual a descrição do fundo ?
    - Qual o valor da cota ?
    - Qual a quantidade de cotas disponíveis ?
    - Qual a quantidade mínima de cotas ?
    
  - Listar Fundos
    Exemplo:
      FUNDO ABC FII
      COD:A123
      TIPO: ACAO
      VALOR DA COTA: R$ 100,00
      QTD COTAS TOTAIS: 1000
      DISPONÍVEIS: 10
      CLIENTES:
        JOÃO, QTD COTAS = 10
        PEDRO, QTD COTAS = 40
      
  - Remover fundo
    - Qual o código do fundo ? (Só pode remover o fundo se não houver cliente)
   
  - Aplicar no fundo
    - Qual o cpf do cliente ? (Se cliente já existente apenas adiciona cotas para ele, caso contrário deve perguntar 
    o nome e cpf para cadastro do cliente)
      - Qual o nome e cpf do cliente ?
    - Qual a quantidade de cotas desejadas ? (Só permite adicionar se a quantidade de cotas atender ao mínimo e a 
    quantidade disponível)
    
  - Resgatar do fundo
    - Qual o cpf do cliente ? (Se cliente não existir deve apontar erro)
    - Qual a quantidade de cotas para resgatar ? (Só permite resgatar se o cliente tiver quantidade desejada, após 
    resgate total deve remover cliente do  fundo)